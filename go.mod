module github.com/diamondburned/handy

go 1.13

require (
	github.com/d2r2/gotk3 v0.0.0-20191124065233-86f7ea85405b
	github.com/gotk3/gotk3 v0.4.1-0.20200321173312-c4ae30c61acd
)
